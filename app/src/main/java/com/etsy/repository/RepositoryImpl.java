package com.etsy.repository;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.etsy.database.DatabaseHelper;
import com.etsy.models.CategoryModel;
import com.etsy.models.ProductModel;
import com.etsy.models.ProductsModel;
import com.etsy.net.EtsyApi;
import com.etsy.net.responses.CategoriesResponse;
import com.etsy.net.responses.SearchResultResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


/**
 * Repository implementation
 *
 * @author dn261290kam on 12/22/2016.
 */
public class RepositoryImpl implements Repository {

    private static final String key = "l6pdqjuf7hdf97h1yvzadfce";

    private EtsyApi api;
    private SQLiteDatabase database;

    public RepositoryImpl(EtsyApi api, DatabaseHelper databaseHelper) {
        this.api = api;
        this.database = databaseHelper.getWritableDatabase();
    }

    @Override
    public Observable<List<CategoryModel>> getCategories() {
        return api.getCategories(key)
                .subscribeOn(Schedulers.io())
                .map(response -> {
                   List<CategoryModel> categories = new ArrayList<>();

                    for (CategoriesResponse.Category c : response.getCategories()) {
                        categories.add(new CategoryModel(c.getName(), c.getShortName()));
                    }

                    return categories;
                });
    }

    @Override
    public Observable<ProductsModel> search(String category, String keywords, int offset) {
        return api.search(key, category, keywords, 10, offset)
                .subscribeOn(Schedulers.io())
                .map(result -> {
                    List<ProductModel> results = new ArrayList<>();

                    for (SearchResultResponse.Result r : result.getResults()) {
                        results.add(new ProductModel(r.getListingId(), r.getTitle(), r.getImageUrl(), r.getDescription(), Double.parseDouble(r.getPrice()), r.getCurrencyCode()));
                    }
                    return new ProductsModel(result.getCount(), results);
                });
    }

    @Override
    public Observable<ProductsModel> getFavorites() {
        return Observable.fromCallable(() -> database.query(DatabaseHelper.TABLE_FAVORITES, null, null, null, null, null, null))
                .subscribeOn(Schedulers.io())
                .map(c -> {
                    List<ProductModel> products = new ArrayList<>(c.getCount());

                    if (c.moveToFirst()) {
                        do {
                            products.add(fromCursorToProductModel(c));
                        } while (c.moveToNext());
                    }


                    return new ProductsModel(products.size(), products);
                });
    }

    @Override
    public Observable<ProductModel> getFavoriteById(long id) {
        return Observable.fromCallable(() -> database.query(DatabaseHelper.TABLE_FAVORITES, null, DatabaseHelper.COLUMNT_ID + " = ?", new String[]{Long.toString(id)}, null, null, null))
                .subscribeOn(Schedulers.io())
                .flatMap(c -> {
                    if (!c.moveToFirst()) {
                        return Observable.empty();
                    }

                    return Observable.just(fromCursorToProductModel(c));
                });
                /*.map(c -> {
                    if (!c.moveToFirst()) {
                        return ;
                    }

                    return fromCursorToProductModel(c);
                })*/
    }

    @Override
    public Observable<Boolean> addFavorite(ProductModel product) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMNT_ID, product.getId());
        cv.put(DatabaseHelper.COLUMNT_TITLE, product.getTitle());
        cv.put(DatabaseHelper.COLUMNT_DESCRIPTION, product.getDescription());
        cv.put(DatabaseHelper.COLUMNT_IMAGE, product.getImageUrl());
        cv.put(DatabaseHelper.COLUMNT_PRICE, product.getPrice());
        cv.put(DatabaseHelper.COLUMNT_CURRENCY, product.getCurrency());

        return Observable.fromCallable(() -> database.insert(DatabaseHelper.TABLE_FAVORITES, "", cv))
                .subscribeOn(Schedulers.io())
                .map(c -> true);
    }

    @Override
    public Observable<Boolean> removeFavorite(ProductModel product) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMNT_ID, product.getId());
        cv.put(DatabaseHelper.COLUMNT_TITLE, product.getTitle());
        cv.put(DatabaseHelper.COLUMNT_DESCRIPTION, product.getDescription());
        cv.put(DatabaseHelper.COLUMNT_IMAGE, product.getImageUrl());
        cv.put(DatabaseHelper.COLUMNT_PRICE, product.getPrice());
        cv.put(DatabaseHelper.COLUMNT_CURRENCY, product.getCurrency());

        return Observable.fromCallable(() -> database.delete(DatabaseHelper.TABLE_FAVORITES, DatabaseHelper.COLUMNT_ID + " = ?", new String[]{Long.toString(product.getId())}))
                .subscribeOn(Schedulers.io())
                .map(c -> true);
    }

    private ProductModel fromCursorToProductModel(Cursor c) {
        long id = c.getLong(c.getColumnIndex(DatabaseHelper.COLUMNT_ID));
        String title = c.getString(c.getColumnIndex(DatabaseHelper.COLUMNT_TITLE));
        String image = c.getString(c.getColumnIndex(DatabaseHelper.COLUMNT_IMAGE));
        String description = c.getString(c.getColumnIndex(DatabaseHelper.COLUMNT_DESCRIPTION));
        double price = c.getDouble(c.getColumnIndex(DatabaseHelper.COLUMNT_PRICE));
        String currency = c.getString(c.getColumnIndex(DatabaseHelper.COLUMNT_CURRENCY));

        return new ProductModel(id, title, image, description, price, currency);
    }
}
