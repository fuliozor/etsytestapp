package com.etsy.repository;

import com.etsy.models.CategoryModel;
import com.etsy.models.ProductModel;
import com.etsy.models.ProductsModel;

import java.util.List;

import io.reactivex.Observable;


/**
 * @author dn261290kam on 12/22/2016.
 */
public interface Repository {

    Observable<List<CategoryModel>> getCategories();

    Observable<ProductsModel> search(String category, String keywords, int offset);

    Observable<ProductsModel> getFavorites();

    Observable<ProductModel> getFavoriteById(long id);

    Observable<Boolean> addFavorite(ProductModel product);

    Observable<Boolean> removeFavorite(ProductModel product);
}
