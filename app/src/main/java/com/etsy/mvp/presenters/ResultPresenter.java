package com.etsy.mvp.presenters;

import android.support.annotation.NonNull;

import com.etsy.R;
import com.etsy.models.ProductModel;
import com.etsy.models.ProductsModel;
import com.etsy.mvp.views.ResultView;
import com.etsy.repository.Repository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by khome on 12/25/2016.
 */
public class ResultPresenter extends BasePresenter<ResultView> {

    private Repository repository;

    private List<ProductModel> items;
    private long count;

    private String category;
    private String keywords;

    private Disposable disposable = Disposables.disposed();

    public ResultPresenter(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void registerView(@NonNull ResultView view) {
        super.registerView(view);

        if (items != null && disposable.isDisposed()) {
            view.showSearchResults(items, count);
        } else {
            items = new ArrayList<>();
            view.showProgress();
        }
    }

    public void onSearch(@NonNull String category, @NonNull String keywords) {
        ResultView view = getView();

        if (view == null) {
            return;
        }

        this.category = category;
        this.keywords = keywords;

        view.showProgress();

        items.clear();

        disposable = repository.search(category, keywords, 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSearchResults, this::onError);
    }

    public void nextItems() {
        ResultView view = getView();

        if (view == null) {
            return;
        }

        if (count > items.size() && disposable.isDisposed()) {

            view.showProgress();

            disposable = repository.search(category, keywords, items.size())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onSearchResults, this::onError);
        }
    }

    public void onItemClick(ProductModel product) {
        ResultView view = getView();
        if (view != null) {
            view.goToDetailsActivity(product);
        }
    }

    private void onError(Throwable throwable) {
        ResultView view = getView();

        if (view == null) {
            return;
        }

        view.showError(R.string.error_internet_not_available);
    }

    private void onSearchResults(ProductsModel products) {

        if (this.items == null) {
            this.items = new ArrayList<>();
        }

        this.items.addAll(products.getProducts());
        this.count = products.getCount();

        ResultView view = getView();

        if (view == null) {
            return;
        }

        view.showSearchResults(items, count);
    }
}
