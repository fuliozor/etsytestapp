package com.etsy.mvp.presenters;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.etsy.EtsyApp;
import com.etsy.R;
import com.etsy.models.CategoryModel;
import com.etsy.mvp.views.SearchView;
import com.etsy.repository.Repository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author dn261290kam on 12/22/2016.
 */
public class SearchPresenter extends BasePresenter<SearchView> {

    private Repository repository;

    private List<CategoryModel> categories;

    private String category;

    public SearchPresenter(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void registerView(@NonNull SearchView view) {
        super.registerView(view);

        downloadCategories();
    }

    public void onRetryDownloadCategories() {
        downloadCategories();
    }

    public void onCategoryChoose(CategoryModel category) {
        this.category = category.getId();
    }

    public void onSearchButtonClick(String keyword) {
        SearchView view = getView();

        if (view == null) {
            return;
        }

        boolean error = false;

        if (TextUtils.isEmpty(category)) {
            view.showErrorForCategory(R.string.search_choose_category);

            error = true;
        }

        //remove all whitespaces
        keyword = keyword.trim();

        if (TextUtils.isEmpty(keyword)) {
            view.showErrorForSearchField(R.string.search_empty_search_string);
            error = true;
        }

        if (error) return;

        view.goToSearchResultActivity(category, keyword);
    }

    private void downloadCategories() {
        SearchView view = getView();

        if (view == null) return;

        if (categories == null || categories.isEmpty()) {
            view.showProgress();

            repository.getCategories()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onCategoriesDownloaded, this::onError);
        } else {
            view.showSearchView(categories);
        }
    }

    private void onError(Throwable throwable) {
        SearchView view = getView();

        if (view == null) return;

        view.showError(R.string.error_internet_not_available);
    }

    private void onCategoriesDownloaded(List<CategoryModel> categories) {
        this.categories = categories;

        categories.add(0, new CategoryModel(null, EtsyApp.getInstance().getString(R.string.search_choose_category)));

        SearchView view = getView();

        if (view == null) return;

        view.showSearchView(categories);
    }
}
