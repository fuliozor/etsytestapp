package com.etsy.mvp.presenters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author dn261290kam on 09/14/2016.
 */
public class BasePresenter<T> {

    private T view;

    @Nullable
    public T getView() {
        return view;
    }

    public void registerView(@NonNull T view) {
        this.view = view;
    }

    public void unregisterView(@Nullable T view) {
        this.view = null;
    }
}
