package com.etsy.mvp.views;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.etsy.models.ProductModel;

import java.util.List;

/**
 * View for screen with search results
 * Created by khome on 12/25/2016.
 */
public interface ResultView {
    void showProgress();

    void showSearchResults(@NonNull List<ProductModel> items, long count);

    void showError(@StringRes int resId);

    void goToDetailsActivity(@NonNull ProductModel product);
}
