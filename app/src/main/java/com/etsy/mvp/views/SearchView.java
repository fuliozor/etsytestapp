package com.etsy.mvp.views;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.etsy.models.CategoryModel;

import java.util.List;

/**
 * View for search screen
 *
 * @author dn261290kam on 12/22/2016.
 */
public interface SearchView {

    void showProgress();

    void showSearchView(@NonNull List<CategoryModel> categories);

    void showError(@StringRes int resId);

    void showErrorForSearchField(@StringRes int resId);

    void goToSearchResultActivity(String category, String search);

    void showErrorForCategory(@StringRes int resId);
}
