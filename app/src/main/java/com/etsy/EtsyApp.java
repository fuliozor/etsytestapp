package com.etsy;

import android.app.Application;

import com.etsy.database.DatabaseHelper;
import com.etsy.mvp.presenters.BasePresenter;
import com.etsy.net.EtsyApi;
import com.etsy.repository.Repository;
import com.etsy.repository.RepositoryImpl;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author dn261290kam on 12/22/2016.
 */
public class EtsyApp extends Application {

    private static final String BASE_URL = "https://openapi.etsy.com/";

    private static EtsyApp instance;

    private Map<String, BasePresenter> presentersCache;

    private Repository repository;

    public static EtsyApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        presentersCache = new HashMap<>();

        Cache cache = new Cache(new File(getCacheDir(), "cache"), 50 * 1024 * 1024);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.cache(cache);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }

        OkHttpClient client = builder.build();

        initPicasso(client);

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation()
                        .create()))
                .baseUrl(BASE_URL)
                .build();

        repository = new RepositoryImpl(retrofit.create(EtsyApi.class), new DatabaseHelper(this));
    }

    public BasePresenter getPresenter(String who) {
        return presentersCache.get(who);
    }

    public void addPresenterToCache(String who, BasePresenter presenter) {
        presentersCache.put(who, presenter);
    }

    public Repository getRepository() {
        return repository;
    }

    protected void initPicasso(OkHttpClient client) {
        Picasso.setSingletonInstance(new Picasso
                .Builder(this)
                .downloader(new OkHttp3Downloader(client))
                .indicatorsEnabled(BuildConfig.DEBUG)
                .build());
    }
}
