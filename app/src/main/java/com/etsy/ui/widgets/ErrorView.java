package com.etsy.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.etsy.R;

/**
 * @author dn261290kam on 05/24/2016.
 */
public class ErrorView extends LinearLayout {

    private TextView tvError;
    private Button btnError;

    public ErrorView(Context context) {
        super(context);

        initView(context, null);
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initView(context, attrs);
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView(context, attrs);
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.widget_error, this, true);

        tvError = (TextView) findViewById(R.id.tvError);
        btnError = (Button) findViewById(R.id.btnError);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ErrorView);

        CharSequence errorText = a.getText(R.styleable.ErrorView_errorText);
        CharSequence buttonText = a.getText(R.styleable.ErrorView_buttonText);

        a.recycle();

        if (!TextUtils.isEmpty(buttonText)) {
            btnError.setText(buttonText);
            btnError.setVisibility(VISIBLE);
        }

        tvError.setText(errorText);
    }

    public void setOnClickListener(OnClickListener listener) {
        btnError.setOnClickListener(listener);
    }

    public void setErrorText(CharSequence text) {
        tvError.setText(text);
    }

    public void setErrorText(@StringRes int text) {
        tvError.setText(text);
    }

    public void setButtonError(CharSequence text) {
        btnError.setText(text);
    }

    public void setButtonError(@StringRes int text) {
        btnError.setText(text);
    }

    public void hideButton() {
        btnError.setVisibility(GONE);
    }
}
