package com.etsy.ui.fragments;

import android.support.v4.app.Fragment;

/**
 * Base fragment for main activity
 *
 * @author dn261290kam on 12/21/2016.
 */
public class BaseFragment extends Fragment {
}
