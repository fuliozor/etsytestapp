package com.etsy.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.etsy.EtsyApp;
import com.etsy.R;
import com.etsy.ui.activities.DetailsActivity;
import com.etsy.ui.adapters.ProductsAdapter;
import com.etsy.ui.widgets.ErrorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * @author dn261290kam on 12/28/2016.
 */
public class FavoriteFragment extends BaseFragment {
    private static final int COLUMN_COUNT = 2;

    @BindView(R.id.errorView)
    protected ErrorView errorView;
    @BindView(R.id.recyclerView)
    protected RecyclerView recyclerView;

    private ProductsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        ButterKnife.bind(this, view);

        adapter = new ProductsAdapter(null, model -> startActivity(new Intent(getContext(), DetailsActivity.class)
                .putExtra(DetailsActivity.KEY_PRODUCT, model)));

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), COLUMN_COUNT));
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        EtsyApp.getInstance().getRepository().getFavorites()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(p -> {
                    errorView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);

                    adapter.clearProducts();

                    if (p.getCount() == 0) {
                        errorView.setErrorText(R.string.favorite_empty_favorites);
                        errorView.hideButton();
                        errorView.setVisibility(View.VISIBLE);
                        return;
                    }

                    adapter.clearAndAddProducts(p.getProducts(), p.getCount());
                    adapter.notifyDataSetChanged();
                    recyclerView.setVisibility(View.VISIBLE);
                });
    }
}
