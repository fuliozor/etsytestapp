package com.etsy.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.etsy.EtsyApp;
import com.etsy.R;
import com.etsy.models.CategoryModel;
import com.etsy.mvp.presenters.SearchPresenter;
import com.etsy.mvp.views.SearchView;
import com.etsy.ui.activities.ResultsActivity;
import com.etsy.ui.widgets.ErrorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author dn261290kam on 12/22/2016.
 */
public class SearchFragment extends BaseFragment implements SearchView {
    private static final String TAG = SearchFragment.class.getSimpleName();

    private static final String KEY_CATEGORY = "CATEGORY";
    private static final String KEY_KEYWORDS = "KEYWORDS";

    @BindView(R.id.includeSearch)
    protected View searchForm;
    @BindView(R.id.errorView)
    protected ErrorView errorView;
    @BindView(R.id.progressBar)
    protected View progressBar;

    /*includeSearch*/
    @BindView(R.id.spCategories)
    protected Spinner spinner;
    @BindView(R.id.tilKeywords)
    protected TextInputLayout tilKeywords;

    private SearchPresenter presenter;

    private int lastSelectedCategory = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        ButterKnife.bind(this, view);

        if (savedInstanceState != null) {
            tilKeywords.getEditText().setText(savedInstanceState.getString(KEY_KEYWORDS, null));
            lastSelectedCategory = savedInstanceState.getInt(KEY_CATEGORY, 0);
        }

        errorView.setOnClickListener(v -> presenter.onRetryDownloadCategories());

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        presenter = (SearchPresenter) EtsyApp.getInstance().getPresenter(TAG);

        if (presenter == null) {
            presenter = new SearchPresenter(EtsyApp.getInstance().getRepository());
            EtsyApp.getInstance().addPresenterToCache(TAG, presenter);
        }

        presenter.registerView(this);

        spinner.setSelection(lastSelectedCategory);
    }

    @Override
    public void onStop() {
        super.onStop();

        presenter.unregisterView(this);
    }

    public void showProgress() {
        hideAllViews();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSearchView(@NonNull List<CategoryModel> categories) {
        hideAllViews();

        ArrayAdapter<CategoryModel> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                lastSelectedCategory = position;
                presenter.onCategoryChoose(adapter.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        searchForm.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(@StringRes int resId) {
        hideAllViews();

        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorForSearchField(@StringRes int resId) {
        tilKeywords.setError(getString(resId));
    }

    @Override
    public void goToSearchResultActivity(String category, String search) {
        startActivity(new Intent(getContext(), ResultsActivity.class)
                .putExtra(ResultsActivity.KEY_CATEGORY, category)
                .putExtra(ResultsActivity.KEY_KEYWORDS, search));
    }

    @Override
    public void showErrorForCategory(@StringRes int resId) {
        Snackbar.make(getView(), R.string.search_choose_category, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_CATEGORY, lastSelectedCategory);
        outState.putString(KEY_KEYWORDS, tilKeywords.getEditText().getText().toString());
    }

    @OnClick(R.id.btnSearch)
    protected void onSearchClick() {
        String keywords = tilKeywords.getEditText().getText().toString();

        presenter.onSearchButtonClick(keywords);
    }

    private void hideAllViews() {
        searchForm.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }


}
