package com.etsy.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.etsy.EtsyApp;
import com.etsy.R;
import com.etsy.models.ProductModel;
import com.etsy.mvp.presenters.ResultPresenter;
import com.etsy.mvp.views.ResultView;
import com.etsy.ui.adapters.ProductsAdapter;
import com.etsy.ui.widgets.ErrorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khome on 12/25/2016.
 */
public class ResultsActivity extends BaseActivity implements ResultView {
    private static final String TAG = ResultsActivity.class.getSimpleName();

    private static final int COLUMN_COUNT = 2;

    public static final String KEY_CATEGORY = "CATEGORY";
    public static final String KEY_KEYWORDS = "KEYWORDS";

    @BindView(R.id.swipeRefreshLayout)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.errorView)
    protected ErrorView errorView;
    @BindView(R.id.recyclerView)
    protected RecyclerView recyclerView;

    private ResultPresenter presenter;

    private ProductsAdapter adapter;

    private String category;
    private String keywords;

    private boolean newSearch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_result);

        ButterKnife.bind(this, this);

        category = getIntent().getStringExtra(KEY_CATEGORY);
        keywords = getIntent().getStringExtra(KEY_KEYWORDS);

        swipeRefreshLayout.setOnRefreshListener(this::search);

        adapter = new ProductsAdapter(this::needMoreData, model -> presenter.onItemClick(model));

        recyclerView.setLayoutManager(new GridLayoutManager(this, COLUMN_COUNT));
        recyclerView.setAdapter(adapter);

        newSearch = savedInstanceState == null;
    }

    private void needMoreData(int count) {
        presenter.nextItems();
    }

    @Override
    protected void onStart() {
        super.onStart();

        presenter = (ResultPresenter) EtsyApp.getInstance().getPresenter(TAG);

        if (presenter == null) {
            presenter = new ResultPresenter(EtsyApp.getInstance().getRepository());
            EtsyApp.getInstance().addPresenterToCache(TAG, presenter);
        }

        presenter.registerView(this);

        if (newSearch) {
            newSearch = false;
            search();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        presenter.unregisterView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void showSearchResults(@NonNull List<ProductModel> products, long count) {
        swipeRefreshLayout.setRefreshing(false);

        if (products.isEmpty()) {
            errorView.setErrorText(getString(R.string.result_empty_result, keywords));
            errorView.hideButton();
            errorView.setVisibility(View.VISIBLE);
            return;
        }

        if (recyclerView.getVisibility() != View.VISIBLE) {
            hideAllViews();
            recyclerView.setVisibility(View.VISIBLE);
        }

        adapter.clearAndAddProducts(products, count);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showError(@StringRes int resId) {
        hideAllViews();

        errorView.setErrorText(resId);
        errorView.setVisibility(View.VISIBLE);

        swipeRefreshLayout.setRefreshing(false);
    }

    public void goToDetailsActivity(ProductModel product) {
        startActivity(new Intent(this, DetailsActivity.class).putExtra(DetailsActivity.KEY_PRODUCT, product));
    }

    private void search() {
        adapter.clearProducts();
        presenter.onSearch(category, keywords);
    }

    private void hideAllViews() {
        recyclerView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
    }
}
