package com.etsy.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.etsy.EtsyApp;
import com.etsy.R;
import com.etsy.models.ProductModel;
import com.etsy.repository.Repository;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Created by khome on 12/28/2016.
 */
public class DetailsActivity extends BaseActivity {
    public static final String KEY_PRODUCT = "PRODUCT";
    private static final String TAG = DetailsActivity.class.getSimpleName();
    @BindView(R.id.imageView)
    protected ImageView imageView;
    @BindView(R.id.tvDescription)
    protected TextView tvDescription;
    @BindView(R.id.tvPrice)
    protected TextView tvPrice;
    @BindView(R.id.tvTitle)
    protected TextView tvTitle;

    private Menu menu;

    private ProductModel product;
    private Boolean favorite;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);

        product = (ProductModel) getIntent().getSerializableExtra(KEY_PRODUCT);

        Picasso.with(this).load(product.getImageUrl()).into(this.imageView);

        tvTitle.setText(Html.fromHtml(product.getTitle()));

        tvPrice.setText(String.format(Locale.getDefault(), "%.2f %s", new Object[]{Double.valueOf(product.getPrice()), product.getCurrency()}));

        tvDescription.setText(Html.fromHtml(product.getDescription()));

        EtsyApp.getInstance().getRepository()
                .getFavoriteById(product.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.e(TAG, "onSubscribe");
                    }

                    @Override
                    public void onNext(ProductModel value) {
                        favorite = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError", e);
                    }

                    @Override
                    public void onComplete() {
                        if(favorite == null) {
                            favorite = false;
                        }

                        setFavoriteIcon(favorite);
                    }
                });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        this.menu = menu;
        menu.getItem(0).setVisible(false);
        setFavoriteIcon(favorite);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.action_add) {
            return super.onOptionsItemSelected(item);
        }

        item.setVisible(favorite);

        Repository repository = EtsyApp.getInstance().getRepository();

        Observable<Boolean> observable;

        if (favorite) {
            observable = repository.removeFavorite(product);
        } else {
            observable = repository.addFavorite(product);
        }

        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(b -> {
                    favorite = !favorite;
                    setFavoriteIcon(favorite);
                });

        setFavoriteIcon(favorite);

        return true;
    }

    private void setFavoriteIcon(boolean isFavorite) {
        if(menu == null || favorite == null) return;

        this.menu.getItem(0)
                .setVisible(true)
                .setIcon(isFavorite ? R.drawable.ic_favorite_remove : R.drawable.ic_favorite_add);
    }
}
