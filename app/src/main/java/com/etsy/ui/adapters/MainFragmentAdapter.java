package com.etsy.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for view pager on MainActivity
 *
 * @author dn261290kam on 12/21/2016.
 */
public class MainFragmentAdapter extends FragmentPagerAdapter {
    private final List<Fragment> fragments;
    private final List<String> fragmentsTitle;

    public MainFragmentAdapter(FragmentManager fm) {
        super(fm);

        fragments = new ArrayList<>(2);
        fragmentsTitle = new ArrayList<>(2);
    }

    public void addFragment(Fragment fragment, String tittle) {
        fragments.add(fragment);
        fragmentsTitle.add(tittle);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentsTitle.get(position);
    }
}
