package com.etsy.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.etsy.R;
import com.etsy.models.ProductModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khome on 12/27/2016.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {
    private static final int threshold = 4;

    private long count;

    private OnNeedMoreData onNeedMoreDataListener;
    private OnItemClickListener itemClickListener;

    private List<ProductModel> products;

    public ProductsAdapter(OnNeedMoreData onNeedMoreDataListener, OnItemClickListener itemClickListener) {
        this.onNeedMoreDataListener = onNeedMoreDataListener;
        this.itemClickListener = itemClickListener;

        this.products = new ArrayList<>();
    }

    private ProductModel getItem(int position) {
        return products.get(position);
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);

        return new ProductViewHolder(v, itemClickListener);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        if (position >= getItemCount() - threshold && getItemCount() < this.count && onNeedMoreDataListener != null) {
            onNeedMoreDataListener.onNeedMoreData(getItemCount());
        }

        holder.setData(getItem(position));
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void clearAndAddProducts(Collection<ProductModel> products, long count) {
        this.count = count;
        this.products.clear();
        this.products.addAll(products);
    }

    public void clearProducts() {
        this.count = 0;
        this.products.clear();
    }

    public interface OnNeedMoreData {
        void onNeedMoreData(int i);
    }

    public interface OnItemClickListener {
        void onItemClickListener(ProductModel model);
    }

    protected class ProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        protected TextView tvTitle;
        @BindView(R.id.imageView)
        protected ImageView imageView;

        private ProductModel model;

        public ProductViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);

            itemView.setOnClickListener(v -> listener.onItemClickListener(model));

            ButterKnife.bind(this, itemView);
        }

        public void setData(ProductModel model) {
            this.model = model;

            Picasso.with(imageView.getContext()).load(model.getImageUrl()).into(imageView);

            tvTitle.setText(Html.fromHtml(model.getTitle()));
        }
    }
}
