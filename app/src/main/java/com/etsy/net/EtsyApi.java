package com.etsy.net;

import com.etsy.net.responses.CategoriesResponse;
import com.etsy.net.responses.SearchResultResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Retrofit Etsi API
 *
 * @author dn261290kam on 12/21/2016.
 */
public interface EtsyApi {

    @GET("/v2/taxonomy/categories")
    Observable<CategoriesResponse> getCategories(@Query("api_key") String key);

    @GET("/v2/listings/active?includes=Images")
    Observable<SearchResultResponse> search(@Query("api_key") String key, @Query("category") String category, @Query("keywords") String keywords, @Query("limit") int limit, @Query("offset") int offset);
}
