package com.etsy.net.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by khome on 12/25/2016.
 */
public class CategoriesResponse {

    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("results")
    @Expose
    private List<Category> categories = null;
    @SerializedName("params")
    @Expose
    private Object params;
    @SerializedName("type")
    @Expose
    private String type;

    public List<Category> getCategories() {
        return categories;
    }

    public class Category {

        @SerializedName("category_id")
        @Expose
        private int categoryId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("meta_title")
        @Expose
        private String metaTitle;
        @SerializedName("meta_keywords")
        @Expose
        private String metaKeywords;
        @SerializedName("meta_description")
        @Expose
        private String metaDescription;
        @SerializedName("page_description")
        @Expose
        private String pageDescription;
        @SerializedName("page_title")
        @Expose
        private String pageTitle;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("short_name")
        @Expose
        private String shortName;
        @SerializedName("long_name")
        @Expose
        private String longName;
        @SerializedName("num_children")
        @Expose
        private int numChildren;

        public String getCategoryName() {
            return categoryName;
        }

        public String getName() {
            return name;
        }

        public String getShortName() {
            return shortName;
        }
    }
}
