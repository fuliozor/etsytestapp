package com.etsy.net.responses;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResultResponse {
    @SerializedName("count")
    @Expose
    private long count;
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;
    @SerializedName("results")
    @Expose
    private List<Result> results;
    @SerializedName("type")
    @Expose
    private String type;

    public class Image {
        @SerializedName("blue")
        @Expose
        private long blue;
        @SerializedName("brightness")
        @Expose
        private long brightness;
        @SerializedName("creation_tsz")
        @Expose
        private long creationTsz;
        @SerializedName("full_height")
        @Expose
        private long fullHeight;
        @SerializedName("full_width")
        @Expose
        private long fullWidth;
        @SerializedName("green")
        @Expose
        private long green;
        @SerializedName("hex_code")
        @Expose
        private String hexCode;
        @SerializedName("hue")
        @Expose
        private long hue;
        @SerializedName("is_black_and_white")
        @Expose
        private boolean isBlackAndWhite;
        @SerializedName("listing_id")
        @Expose
        private long listingId;
        @SerializedName("listing_image_id")
        @Expose
        private long listingImageId;
        @SerializedName("rank")
        @Expose
        private long rank;
        @SerializedName("red")
        @Expose
        private long red;
        @SerializedName("saturation")
        @Expose
        private long saturation;
        @SerializedName("url_170x135")
        @Expose
        private String url170x135;
        @SerializedName("url_570xN")
        @Expose
        private String url570xN;
        @SerializedName("url_75x75")
        @Expose
        private String url75x75;
        @SerializedName("url_fullxfull")
        @Expose
        private String urlFullxfull;

        public String getUrlFullxfull() {
            return this.urlFullxfull;
        }

        public String getUrl170x135() {
            return this.url170x135;
        }
    }

    public class Pagination {
        @SerializedName("effective_limit")
        @Expose
        private long effectiveLimit;
        @SerializedName("effective_offset")
        @Expose
        private long effectiveOffset;
        @SerializedName("effective_page")
        @Expose
        private long effectivePage;
        @SerializedName("next_offset")
        @Expose
        private long nextOffset;
        @SerializedName("next_page")
        @Expose
        private long nextPage;
    }

    public class Result {
        @SerializedName("category_id")
        @Expose
        private long categoryId;
        @SerializedName("category_path")
        @Expose
        private List<String> categoryPath;
        @SerializedName("category_path_ids")
        @Expose
        private List<Integer> categoryPathIds;
        @SerializedName("creation_tsz")
        @Expose
        private long creationTsz;
        @SerializedName("currency_code")
        @Expose
        private String currencyCode;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("ending_tsz")
        @Expose
        private long endingTsz;
        @SerializedName("file_data")
        @Expose
        private String fileData;
        @SerializedName("has_variations")
        @Expose
        private boolean hasVariations;
        @SerializedName("Images")
        @Expose
        private List<Image> images;
        @SerializedName("is_customizable")
        @Expose
        private boolean isCustomizable;
        @SerializedName("is_digital")
        @Expose
        private boolean isDigital;
        @SerializedName("is_private")
        @Expose
        private boolean isPrivate;
        @SerializedName("is_supply")
        @Expose
        private String isSupply;
        @SerializedName("item_dimensions_unit")
        @Expose
        private String itemDimensionsUnit;
        @SerializedName("item_height")
        @Expose
        private String itemHeight;
        @SerializedName("item_length")
        @Expose
        private String itemLength;
        @SerializedName("item_weight")
        @Expose
        private String itemWeight;
        @SerializedName("item_width")
        @Expose
        private String itemWidth;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("last_modified_tsz")
        @Expose
        private long lastModifiedTsz;
        @SerializedName("listing_id")
        @Expose
        private long listingId;
        @SerializedName("materials")
        @Expose
        private List<Object> materials;
        @SerializedName("non_taxable")
        @Expose
        private boolean nonTaxable;
        @SerializedName("num_favorers")
        @Expose
        private long numFavorers;
        @SerializedName("original_creation_tsz")
        @Expose
        private long originalCreationTsz;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("processing_max")
        @Expose
        private long processingMax;
        @SerializedName("processing_min")
        @Expose
        private long processingMin;
        @SerializedName("quantity")
        @Expose
        private long quantity;
        @SerializedName("shipping_template_id")
        @Expose
        private long shippingTemplateId;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("state_tsz")
        @Expose
        private long stateTsz;
        @SerializedName("taxonomy_id")
        @Expose
        private long taxonomyId;
        @SerializedName("taxonomy_path")
        @Expose
        private List<String> taxonomyPath;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("used_manufacturer")
        @Expose
        private boolean usedManufacturer;
        @SerializedName("user_id")
        @Expose
        private long userId;
        @SerializedName("views")
        @Expose
        private long views;
        @SerializedName("when_made")
        @Expose
        private String whenMade;
        @SerializedName("who_made")
        @Expose
        private String whoMade;

        public Result() {
            this.categoryPath = null;
            this.categoryPathIds = null;
            this.materials = null;
            this.taxonomyPath = null;
        }

        public String getTitle() {
            return this.title;
        }

        public long getListingId() {
            return this.listingId;
        }

        public String getDescription() {
            return this.description;
        }

        public String getCurrencyCode() {
            return this.currencyCode;
        }

        public String getPrice() {
            return this.price;
        }

        @Nullable
        public String getImageUrl() {
            if (this.images.isEmpty()) {
                return null;
            }
            return this.images.get(0).getUrl170x135();
        }
    }

    public SearchResultResponse() {
        this.results = null;
    }

    public List<Result> getResults() {
        return this.results;
    }

    public long getCount() {
        return this.count;
    }
}