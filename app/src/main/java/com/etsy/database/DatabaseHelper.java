package com.etsy.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author dn261290kam on 12/28/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String TABLE_FAVORITES = "favorites";

    public static final String COLUMNT_ID = "id";
    public static final String COLUMNT_TITLE = "title";
    public static final String COLUMNT_DESCRIPTION = "description";
    public static final String COLUMNT_IMAGE = "image";
    public static final String COLUMNT_PRICE = "price";
    public static final String COLUMNT_CURRENCY = "currency";

    private static final String databaseName = "etsy";
    private static final int version = 1;

    public DatabaseHelper(Context context) {
        // конструктор суперкласса
        super(context, databaseName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // создаем таблицу с полями
        db.execSQL("create table " + TABLE_FAVORITES + " ("
                + COLUMNT_ID + " integer primary key,"
                + COLUMNT_TITLE + " text,"
                + COLUMNT_DESCRIPTION + " text,"
                + COLUMNT_IMAGE + " text,"
                + COLUMNT_PRICE + " real,"
                + COLUMNT_CURRENCY + " text" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
