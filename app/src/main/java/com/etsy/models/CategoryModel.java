package com.etsy.models;

/**
 * @author dn261290kam on 12/22/2016.
 */
public class CategoryModel {

    private String id;
    private String title;

    public CategoryModel(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return title;
    }
}
