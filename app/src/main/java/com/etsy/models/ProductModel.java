package com.etsy.models;

import java.io.Serializable;

/**
 * Created by khome on 12/25/2016.
 */
public class ProductModel implements Serializable {
    private String currency;
    private String description;
    private long id;
    private String imageUrl;
    private double price;
    private String title;

    public ProductModel(long id, String title, String imageUrl, String description, double price, String currency) {
        this.id = id;
        this.title = title;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.currency = currency;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public long getId() {
        return this.id;
    }

    public double getPrice() {
        return this.price;
    }

    public String getCurrency() {
        return this.currency;
    }
}