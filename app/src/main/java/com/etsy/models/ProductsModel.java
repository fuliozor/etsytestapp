package com.etsy.models;

import java.util.List;

/**
 * Created by khome on 12/28/2016.
 */
public class ProductsModel {
    private long count;
    private List<ProductModel> products;

    public ProductsModel(long count, List<ProductModel> products) {
        this.count = count;
        this.products = products;
    }

    public long getCount() {
        return this.count;
    }

    public List<ProductModel> getProducts() {
        return this.products;
    }
}
