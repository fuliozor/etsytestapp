package com.etsy.integrated;

import com.etsy.BuildConfig;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.plugins.RxJavaPlugins;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, application = TestEtsyApp.class)
@Ignore
public class BaseIntegratedTest {

    @Before
    public void setUp() {
        RxJavaPlugins.setInitIoSchedulerHandler(callable -> AndroidSchedulers.mainThread());
    }
}
