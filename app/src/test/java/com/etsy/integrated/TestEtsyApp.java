package com.etsy.integrated;

import com.etsy.EtsyApp;

import okhttp3.OkHttpClient;

/**
 * Application class for tests
 * @author dn261290kam on 12/28/2016.
 */
public class TestEtsyApp extends EtsyApp{

    @Override
    protected void initPicasso(OkHttpClient client) {
    }
}
