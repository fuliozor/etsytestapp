package com.etsy.integrated;

import com.etsy.R;
import com.etsy.models.ProductModel;
import com.etsy.mvp.presenters.ResultPresenter;
import com.etsy.mvp.views.ResultView;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by khome on 12/25/2016.
 */
public class SearchResultTest extends BaseIntegratedTest {

    private static final String category = "paper_goods";
    private static final String keywords = "terminator";

    private ResultView searchView;

    private TestSuccessRepository successRepository;
    private TestFailureRepository failureRepository;

    @Before
    public void setUp() {
        super.setUp();
        successRepository = new TestSuccessRepository();
        failureRepository = new TestFailureRepository();

        searchView = spy(ResultView.class);
    }

    @Test
    public void testRegistrationView() {
        ResultPresenter presenter = new ResultPresenter(successRepository);

        presenter.registerView(searchView);

        verify(searchView).showProgress();
        verify(searchView, never()).showSearchResults(anyList(), anyLong());
        verify(searchView, never()).showError(R.string.error_internet_not_available);
    }

    @Test
    public void testErrorLoadData() {
        ResultPresenter presenter = new ResultPresenter(failureRepository);

        presenter.registerView(searchView);

        presenter.onSearch(category, keywords);

        verify(searchView, times(2)).showProgress();
        verify(searchView).showError(R.string.error_internet_not_available);
        verify(searchView, never()).showSearchResults(anyList(), anyLong());
    }

    @Test
    public void testLoadData() {
        ResultPresenter presenter = new ResultPresenter(successRepository);

        presenter.registerView(searchView);

        presenter.onSearch(category, keywords);

        verify(searchView, times(2)).showProgress();
        verify(searchView).showSearchResults(anyList(), anyLong());
        verify(searchView, never()).showError(R.string.error_internet_not_available);
    }

    @Test
    public void testLoadDataWithPagination() {
        ResultPresenter presenter = new ResultPresenter(successRepository);

        presenter.registerView(searchView);

        presenter.onSearch(category, keywords);

        verify(searchView, times(2)).showProgress();
        verify(searchView).showSearchResults(anyList(), anyLong());
        verify(searchView, never()).showError(R.string.error_internet_not_available);

        presenter.nextItems();

        verify(searchView, times(3)).showProgress();
        verify(searchView, times(2)).showSearchResults(anyList(), anyLong());
        verify(searchView, never()).showError(R.string.error_internet_not_available);
    }

    @Test
    public void testProductClick() {
        ProductModel product = TestSuccessRepository.products.get(0);

        ResultPresenter presenter = new ResultPresenter(successRepository);

        presenter.registerView(searchView);

        presenter.onSearch(category, keywords);

        presenter.onItemClick(product);

        verify(searchView).goToDetailsActivity(product);
    }
}
