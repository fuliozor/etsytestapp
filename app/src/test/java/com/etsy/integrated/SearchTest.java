package com.etsy.integrated;

import com.etsy.R;
import com.etsy.integrated.BaseIntegratedTest;
import com.etsy.integrated.TestFailureRepository;
import com.etsy.integrated.TestSuccessRepository;
import com.etsy.models.CategoryModel;
import com.etsy.mvp.presenters.SearchPresenter;
import com.etsy.mvp.views.SearchView;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author dn261290kam on 12/22/2016.
 */
public class SearchTest extends BaseIntegratedTest {

    private SearchView searchView;
    private TestSuccessRepository successRepository;
    private TestFailureRepository failureRepository;

    @Before
    public void setUp() {
        super.setUp();
        successRepository = new TestSuccessRepository();
        failureRepository = new TestFailureRepository();

        searchView = spy(SearchView.class);
    }

    @Test
    public void testLoadData() {
        SearchPresenter presenter = new SearchPresenter(successRepository);

        presenter.registerView(searchView);

        verify(searchView).showProgress();
        verify(searchView).showSearchView(anyList());

        presenter.unregisterView(searchView);

        verify(searchView, never()).showError(R.string.error_internet_not_available);
    }

    @Test
    public void testLoadDataAfterUnregisterView() {
        SearchPresenter presenter = new SearchPresenter(successRepository);
        SearchView temp = spy(SearchView.class);

        presenter.registerView(temp);
        presenter.unregisterView(temp);

        presenter.registerView(searchView);

        verify(searchView).showSearchView(anyList());

        verify(searchView, never()).showProgress();
        verify(searchView, never()).showError(R.string.error_internet_not_available);
    }

    @Test
    public void testLoadDataWithError() {
        SearchPresenter presenter = new SearchPresenter(failureRepository);

        presenter.registerView(searchView);

        verify(searchView).showProgress();
        verify(searchView).showError(R.string.error_internet_not_available);
        verify(searchView, never()).showSearchView(TestSuccessRepository.categories);

        presenter.onRetryDownloadCategories();

        verify(searchView, times(2)).showProgress();
        verify(searchView, times(2)).showError(R.string.error_internet_not_available);
        verify(searchView, never()).showSearchView(TestSuccessRepository.categories);
    }

    @Test
    public void testSearchClick() {
        SearchPresenter presenter = new SearchPresenter(successRepository);

        CategoryModel category = TestSuccessRepository.categories.get(1);
        String keyword = "terminator";

        presenter.registerView(searchView);

        presenter.onCategoryChoose(category);

        presenter.onSearchButtonClick(keyword);

        verify(searchView).goToSearchResultActivity(category.getId(), keyword);
        verify(searchView, never()).showErrorForSearchField(R.string.search_empty_search_string);
    }

    @Test
    public void testEmptyCategoryAndKeyword() {
        SearchPresenter presenter = new SearchPresenter(successRepository);

        presenter.registerView(searchView);

        presenter.onSearchButtonClick("");

        verify(searchView).showErrorForSearchField(R.string.search_empty_search_string);
        verify(searchView).showErrorForCategory(R.string.search_choose_category);
        verify(searchView, never()).goToSearchResultActivity(any(), any());
    }

    @Test
    public void testEmptyCategory() {
        SearchPresenter presenter = new SearchPresenter(successRepository);

        String keyword = "terminator";

        presenter.registerView(searchView);

        presenter.onSearchButtonClick(keyword);

        verify(searchView).showErrorForCategory(R.string.search_choose_category);
        verify(searchView, never()).showErrorForSearchField(anyInt());
        verify(searchView, never()).goToSearchResultActivity(any(), any());
    }

    @Test
    public void testEmptyKeyword() {
        SearchPresenter presenter = new SearchPresenter(successRepository);

        CategoryModel category = TestSuccessRepository.categories.get(1);

        presenter.registerView(searchView);

        presenter.onCategoryChoose(category);
        presenter.onSearchButtonClick("");

        verify(searchView).showErrorForSearchField(R.string.search_empty_search_string);
        verify(searchView, never()).showErrorForCategory(anyInt());
        verify(searchView, never()).goToSearchResultActivity(any(), any());
    }
}
