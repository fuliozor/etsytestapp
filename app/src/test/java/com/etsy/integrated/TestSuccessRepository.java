package com.etsy.integrated;

import com.etsy.models.CategoryModel;
import com.etsy.models.ProductModel;
import com.etsy.models.ProductsModel;
import com.etsy.repository.Repository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;


/**
 * Repository for test with success response
 * Created by khome on 12/25/2016.
 */
public class TestSuccessRepository implements Repository {
    public static List<CategoryModel> categories;
    public static List<ProductModel> products;

    static {
        categories = new ArrayList<>();

        categories.add(new CategoryModel("test1", "test1"));
        categories.add(new CategoryModel("test2", "test2"));

        products = new ArrayList<>();

        products.add(new ProductModel(1, "title1", "url1", "description1", 1.0, "EUR"));
        products.add(new ProductModel(2, "title2", "url2", "description1", 2.0, "EUR"));
    }

    @Override
    public Observable<List<CategoryModel>> getCategories() {
        return Observable.just(new ArrayList<>(categories));
    }

    @Override
    public Observable<ProductsModel> search(String category, String keywords, int offset) {
        return Observable.just(new ProductsModel(products.size() * 2, new ArrayList<>(products)));
    }

    @Override
    public Observable<ProductsModel> getFavorites() {
        return null;
    }

    @Override
    public Observable<ProductModel> getFavoriteById(long id) {
        return null;
    }

    @Override
    public Observable<Boolean> addFavorite(ProductModel product) {
        return null;
    }

    @Override
    public Observable<Boolean> removeFavorite(ProductModel product) {
        return null;
    }
}
