package com.etsy.integrated;

import com.etsy.models.CategoryModel;
import com.etsy.models.ProductModel;
import com.etsy.models.ProductsModel;
import com.etsy.repository.Repository;

import java.util.List;

import io.reactivex.Observable;

/**
 * Repository for test with success response
 * Created by khome on 12/25/2016.
 */
public class TestFailureRepository implements Repository {
    @Override
    public Observable<List<CategoryModel>> getCategories() {
        return Observable.error(new Throwable());
    }

    @Override
    public Observable<ProductsModel> search(String category, String keywords, int offset) {
        return Observable.error(new Throwable());
    }

    @Override
    public Observable<ProductsModel> getFavorites() {
        return null;
    }

    @Override
    public Observable<ProductModel> getFavoriteById(long id) {
        return null;
    }

    @Override
    public Observable<Boolean> addFavorite(ProductModel product) {
        return null;
    }

    @Override
    public Observable<Boolean> removeFavorite(ProductModel product) {
        return null;
    }
}
